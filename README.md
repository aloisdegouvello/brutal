# Brutal

Brutal is a simple tool to let Reinhardt focus on pure smashing without intellectual overload

[live](https://aloisdegouvello.gitlab.io/brutal/index.html)